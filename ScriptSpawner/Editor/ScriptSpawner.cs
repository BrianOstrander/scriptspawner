using UnityEngine;
using UnityEditor;
using System.Collections;

namespace AdvancedObjects {
	
	/// <summary>
	/// Adds a button to the context menu to allow the spawning of monobehaviours from the project pane.
	/// </summary>
	public class ScriptSpawner : MonoBehaviour {

		[MenuItem("Assets/Spawn")]
		static void SpawnScript(MenuCommand command) {

			if (Selection.activeObject.GetType().IsAssignableFrom(typeof(MonoScript))) {
				GameObject spawned = new GameObject();
				spawned.AddComponent(Selection.activeObject.name);
				spawned.name = Selection.activeObject.name;
			}
			
		}
	}
}