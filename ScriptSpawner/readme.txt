ScriptSpawner
By Brian Ostrander

This is a plugin for Unity, allowing you to spawn monobehaviours directly from the project pane. Just right-click on a behaviour, and click "spawn" at the bottom, and an instance of that monobehaviour, attached to a gameobject of the same name, will appear in the current scene.